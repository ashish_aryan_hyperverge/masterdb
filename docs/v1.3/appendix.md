Appendices
==========

Appendix A - Errors
-------------------

Our services return below mentioned error codes.

|  HTTP Error Codes |  Meaning |
|  ------------------ | ----------------------------------------------------------------------------- |
|  400           |     Bad Request. |
|  401           |     Unauthorized access. |
|  403           |     Forbidden - The requested URL is hidden for administrators only. |
|  404           |     Not Found - The requested URL cannot be found. |
|  500            |    Internal Server Error -- We had a problem with our server. Try again later. |


| Custom Error | Codes |  Meaning |
| ------------ | ------- | ---------- |
| E001           |      face not detected in one or more images |                                              
| E002           |      input image is missing |
| E003           |      input image is larger than limit |
| E004           |      missing required input params |
| E005           |      invalid image format |
| E006           |      invalid appId |
| E007           |      face recognition call not executed for given appId |
| E008           |      Invalid face transaction id |
| E009           |      Invalid personId |
| E011           |      Upload Error : Unable to parse invalid formdata |
| E014           |      Invalid input data |

Appendix B
----------

With this new enrolment API the following functions will be affected and
the affects are mentioned below:

1.  Merge API

    Would merge the cross sell applications across multiple people being
    merged.

2.  UpdateNational ID API

    The correct state of the cross sell application can be toggled with
    this API.

3.  Get AppId Journey

    The getJourney API can be used to get the appId journey of the cross
    sell applications. The cross sell applications details will also be
    the part of the journey for the usual applications.

4.  Search by PersonId

    Search by personId will start returning the cross sell applications
    apart from the usual applications it holds.

5.  Search by AppId

    This API can be used to search the cross sell applications from the
    database.

6.  1:N face recognie dedupe API

    The current Dedupe API - returns the list of applications the person
    has in the dedupe result. A new field will be added where the list
    of cross sell applications will be returned.

[shell](#)