API Endpoints
=============

The MasterDB API is organised around
[Rest](http://en.wikipedia.org/wiki/Representational_State_Transfer).
Our API has predictable resource-oriented URLs, accepts form-encoded and
raw json body and returns JSON encoded responses and uses standard HTTP
response codes.

All the endpoints are hosted at:

`https://frsapi.fecredit.com.vn/v1.2/`

Face Recognition API
--------------------

**Without Dedupe (De-duplication)**

This API is used to verify if the 2 face images belong to the same person. The 2 face images could be from different sources like National ID, Selfie, webcamera image etc. In case the verification performed is between an id and another image then file labels should be id/dl and selfie, else the labels need to be selfie and otherSelfie.

If the face is detected in both the images, then the response would include the face match score between the two faces along with the confidence of the prediction and a to-be-reviewed flag. The to-be-reviewed flag is used to decide if the response needs to be manually reviewed.

Depending on the FRR (false rejection rate) and FAR (false acceptance rate) required for automated review and willingness to perform manual review, the user can adopt an alternate review mechanism based on the confidence score. This can be arrived at by the user and HyperVerge working together. 

**With Dedupe (De-duplication)**

**In the "Detailed Facial Checking Flow" above, please refer to the steps 3, 4, 5, 6 in green. These steps are performed by the face recognition API when the `dedupe` option is set to true.** Combining these steps prevents repeated upload requests and also redundant conversions of PDF and face extraction from ID cards, hence, will greatly increase the capacity of the system.

If the optional parameter `dedupe` is set to true, then: 

A search is performed in the master face database for faces matching the footprint of the selfie and national id image in the request. The output would include the matches along with the details, this would be present as an array names dedupe-result. N would be identified based on the user's process at a later stage. The details will include the match-score and the confidence of the prediction. The other details along with the face image will also be retrieved for easy manual comparison.

In case dedupe is set to true, then additional parameters such as NID number, Name, DOB, Gender may be provided to perform detection of fraud that is not necessarily captured by face recognition based deduplication.

The **conf score** indicates the confidence of the prediction. If the conf is low, then it indicates that the Machine learning system is less confident of it's prediction. The **match score** indicates the degree to which the 2 faces are similar. If the score is 99 or 100 it means that the face image is exactly the same. Instead of using the match-score the user can rely on the "match" value which would be "yes" or "no", and instead of using the conf score the user can rely on the "to-be-reviewed" value which would be "yes" or "no".

### HTTP Request

`POST /face/recognize`

> Non Dedupe API

```shell
curl --location --request POST 'http://frsapi.fecredit.com.vn/v1.3/face/recognize' \
    --header 'appkey: appKey' --header 'appsecret: appSecret' \
    --header "transactionId: transactionId" --header "referenceId: referenceId" \
    --form 'id=@nid.png' \
    --form 'selfie=@selfie.png'
```

> The above command returns JSON structured like this:

```json
{
    "status" : "success",
    "statusCode" : "200",
    "result" : {
        "verification-result" : [{
            "type" : "id-selfie/selfie-selfie/dl-selfie"
            "match" : "yes/no",
            "match-score" : <number>, // 0-100
            "to-be-reviewed": "yes/no",
            "conf" : <number>, //0-100 it is the confidence with which the prediction is correct
        }],
    }
}
```

### Query Parameters for Non Dedupe API

Parameter | Type | Description
--------- | ------- | -----------
selfie | FILE | The file containing the selfie image
id | FILE | The file containing the nationalId image
dl | FILE | The file containing the driving license image
otherSelfie | FILE | The file contianing some other selfie image

At a given point, the API can accept only the below mentioned combinations:<br>
`selfie-id`, `selfie-dl` or `selfie-otherSelfie`.

> Dedupe API

```shell
curl --location --request POST 'http://frsapi.fecredit.com.vn/v1.3/face/recognize' \
    --header 'appkey: appKey' --header 'appsecret: appSecret' \
    --header "transactionId: transactionId" --header "referenceId: referenceId" \
    --form 'id=@nid.png' \
    --form 'selfie=@selfie.png' \
    --form 'dedupe=true' \
    --form 'nid=1234567880' \
    --form 'name=Huong Son' \
    --form 'dob=26-04-1993' \
    --form 'gender=M' \
    --form 'doi=01-01-2008' \
    --form 'province=TP Ho Chi Minh City' \
    --form 'doe=01/01/2020' \
    --form 'address=Quan 4, Ho Chi Minh City, VN.'
```

> The above command returns JSON structured like this:

```json
{
    "status" : "success",
    "statusCode" : "200",
    "result" : {
        "verification-result" : [{
            "type" : "id-selfie/selfie-selfie/dl-selfie"
            "match" : "yes/no",
            "match-score" : <number>, // 0-100
            "match_score" : <number>, // 0-100
            "to-be-reviewed": "yes/no",
            "conf" : <number>, //0-100 it is the confidence with which the prediction is correct
        }],
        "dedupe-result": [
            {
                "details": [
                {
                    "address": <string>,
                    "appId": <string>,
                    "correctState": true,
                    "dob": "07-08-1992", // DD-MM-YYYY
                    "doi": "22-10-2009", // DD-MM-YYYY
                    "exist": "yes/no",
                    "gender": "M/F",
                    "name": <string>,
                    "nid": <string>,
                    "distance": <number>, // 0 to 1 in decimal
                    "match-score": <number>, // 0 - 100
                    "conf": <number>, // 0 - 100
                    "latestEnrollment" : "yes/no",
                    "matchReason": [
                        "selfieMatch",
                        "nidFaceMatch",
                        "nidNumberMatch",
                        "nameDOBMatch"
                    ] // one or more from these 4 options
                },
                ...
                ],
                "otherAppIds": [
                    <string>, // appId
                    ...
                ],
                "personId": <string>,
                "blacklisted": true/false
            }
        ],
        "face-transaction-id" : <string>
        "dedupe-to-be-reviewed": "yes/no"
    },
    "requestId": <string>
}
```


### Query Parameters for Dedupe API

Parameter | Type | Description
--------- | ------- | -----------
selfie | FILE | The file containing the selfie image
id | FILE | The file containing the nationalId image
dl | FILE | The file containing the driving license image
otherSelfie | FILE | The file contianing some other selfie image
dedupe | BOOL | The default value is true. This needs to be set to `true` to enable dedupe
nid | STRING | National ID Number
name | STRING | Name
dob | STRING | Date of birth
doi | STRING | Date of Issue of the national ID
doe | STRING | Date of Expiry of the national ID
gender | STRING | Gender
province | STRING | Province to where the customer belong
address | STRING | Address of the customer
retBase64Image | BOOL | `true` or `false` to get the matching customer's face in the response


At a given point, the API can accept only the below mentioned combinations:<br>
`selfie-id`, `selfie-dl` or `selfie-otherSelfie`.

<aside class="success">
Incase of successful dedupe, there are 3 possible scenarios in the output.
</aside>

 - Confident positive match: This would return a single object in the dedupe result. This is the case where there is a person with a very similar face in the database.
 - Confident negative match: In this case there would be 0 dedupe results. This is the case when there are no similar people at all.
 - Grey zone with less confidence: Top N dedupe-results would be returned for the purpose of manual review. In this case, there are multiple people in the database who are similar to the applicant and the system is not able to confidently identify if the applicant is present in the database and who it could be.

For each item in dedupe-result, the selfie-image and id-image is returned in the response only if retBase64Image is passed in the request payload and is set to true.

Face mismatch : Additionally, the dedupe API call will also report the applications that have the same National ID number if the selfies donot match.

<aside class="warning">
Below are the possible errors:
</aside>
When a wrong request is made, an HTTP error `400` is returned. And it could be because of below mentioned reasons:

 - No Image or 1 image input: "E002 - (selfie and id) or (selfie and otherSelfie) or (selfie and dl) are required"
 - Larger than allowed image input: "E003 - image size cannot be greater than 6MB"
 - Incorrect formdata upload: "E011 - Upload Error : Unable to parse invalid formdata"

When the request is not authenticated, an HTTP error `401` is returned as described earlier.

```json
{
    "status" : "success",
    "statusCode" : "200",
    "result" : {
        "error" : "E001 - Face not detected in one or more images",
    }
}
```

When a proper request has some issues while processing it, an HTTP status `200` is returned. Errors in such cases are returned in the error body as shown on the right. The list of such errors are detailed in a table at the bottom of the page.

Enrollment API
--------------

Enroll a new user or register a face to an existing user. After a new
applicant is checked for repeat or blacklist user, he is required to be
enrolled into the face database to ensure future search of the database
returns the correct result for the applicant.

In case of an existing user, registering their new image would help to
keep the face database up to date. This is performed by providing the
appId of the new applicant, the face-recognition-transaction-id and the
personId from the face recognition (with dedupe) API response. By
providing the personId, the new appId gets linked to the face in the
personId. The user data and biometric facial footprint will be
referenced from the face-recognition-transaction-id fed as input.

If the user is being registered for the first time and is not to be
blacklisted, the user needs to be enrolled but should not be linked with
any existing user.

If the user who is being registered is to be blacklisted alongwith the
registration, then the variable black needs to be set.

This API supports AppId Jouney. For details, please refer to the AppId
Journey section.

### HTTP Request

`POST /person/enroll`

> To make the API call

```js
    curl --location --request POST 'http://frsapi.fecredit.com.vn/v1.2/person/enroll' \
    --header 'appkey: appKey' --header 'appsecret: appSecret' \
    --header "transactionId: transactionId" --header "referenceId: referenceId" \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "face-recognition-transaction-id": "9504a935-d94a-461e-8c4f-0489f67b1895",
        "appId" : "32",
        "correctState": true,
        "meta": {
            "ch" : "PEGA",
            "ac" : "User Onboarding",
            "uId": "chandraUserId",
            "uCo": "chandra comments",
            "ma" : "yes"
        },
        "profileData": {
           "name": "Name",
           "dob": "DOB",
           "address": "Address",
           ...
        }
    }'
```

> The above command returns JSON structured like this:

```json
{
    "status" : "success",
    "statusCode" : "200",
    "result": {
        "personId" : "some personId from the database"
    }
}
```

### Query Parameters

| Parameter                       |  Type  |   Description |
| ------------------------------- | ------ | ------------- |
| face-recognition-transaction-id |  STRING |  The face transaction ID returned in the Dedupe API Call |
| appId                           |  STRING |  The appId which the `face-recognition-transaction-id` be replaced with |
| black                           |  BOOL   |  (Optional, Default: false) Flag to specify the blacklist flag of the customer |
| personId                        |  STRING |  (Optional) PersonId to which the appId should be added to |
| profileData                     |  JSON   |  (Optional) The profile data shared in this option would be used only for enrolment done after a plain face search. |

A successful API response will give the verification result as shown on
the right.

Below are the possible errors:

When a wrong request is made, an HTTP error `400` is returned. And it
could be because of below mentioned reasons, which is returned in the
status message.

-   Missing/invalid params: "E004 - missing required input params"
-   Invalid transaction id:"E008 - Invalid face transaction id"
-   Invalid personId: "E009 - Invalid personId"
-   E014 - "E014 - Invalid input data for blacklist"

When the request is not authenticated, an HTTP error `401` is returned
as described earlier.

Face Authentication API
-----------------------

This API is used to verify a given selfie image against the image used
at the time of enrollment (refer enroll API). This would be used at a
later stage of a customer life cycle when the customer returns to the
mobile application for a loan disbursement.

### HTTP Request

`POST /face/authenticate`

> To make the API call

```js
    curl --location --request POST 'http://frsapi.fecredit.com.vn/v1.2/face/authenticate' \
    --header 'appkey: appKey' --header 'appsecret: appSecret' \
    --header "transactionId: transactionId" --header "referenceId: referenceId" \
    --form 'personId="personId"' \
    --form 'image=@selfie.png'
```

> The above command returns JSON structured like this:

```json
{
    "status" : "success",
    "statusCode" : "200",
    "result" : {
        "verification-result" : {
            "match" : "yes/no",
            "match-score" : <number>, // 0-100
            "to-be-reviewed": "yes/no",
            "conf" : <number>, //0-100 it is the confidence with which the prediction is correct
        }
    }
}
```

### Query Parameters

|  Parameter |  Type |    Description |
|  ----------- | -------- | ----------------------------------------- |
|  personId   | STRING  | The personId of the customer who has to be authenticated |
|  image      | FILE   |  The file containing the slefie image |

A successful API response will give the verification result as shown on
the right.

Below are the possible errors:

When a wrong request is made, an HTTP error `400` is returned. And it
could be because of below mentioned reasons, which is returned in the
status message.

-   No Image: "E002 - image is required"
-   Larger than allowed image input: "E003 - image size cannot be
    greater than 6MB"
-   Missing Param: "E004 - personId is required"
-   Image format not supported: "E005 - invalid image format"
-   User with the given personId not already enrolled: "E006 - invalid
    personId"
-   Incorrect formdata upload: "E011 - Upload Error : Unable to parse
    invalid formdata"

When the request is not authenticated, an HTTP error `401` is returned
as described earlier.

Quality Check API
-----------------

Used to verify if the selfie image is of sufficient quality to perform
face recognition. If the image is rejected, the user is required to
capture another image. If the image is accepted by the quality check
API, then the user can be assured that the face is detectable in the
image and can proceed to verification or recognition steps.

### HTTP Request

`POST /photo/check`

> To make the API call

```js
    curl --location --request POST 'http://frsapi.fecredit.com.vn/v1.2/photo/check' \
    --header 'appkey: appKey' --header 'appsecret: appSecret' \
    --header "transactionId: transactionId" --header "referenceId: referenceId" \
    --form 'image=@selfie.png'
```

> The above command returns JSON structured like this:

```json
{
    "status" : "success",
    "statusCode" : "200",
    "result" : {
        "quality" : "good/bad"
    }
}
```

### Query Parameters

|  Parameter  | Type |  Description |
|  ----------- | ------ | ----------------------  |
|  image   | FILE  | The file containing the slefie image |

A successful API response will give the verification result as shown on
the right.

Below are the possible errors:

When a wrong request is made, an HTTP error `400` is returned. And it
could be because of below mentioned reasons, which is returned in the
status message.

-   No Image: "E002 - image is compulsory"
-   Larger than allowed image input: "E003 - image size cannot be
    greater than 6MB"
-   Image format not supported: "E005 - supported image types include
    jpeg, tiff, png"
-   Incorrect formdata upload: "E011 - Upload Error : Unable to parse
    invalid formdata"

When the request is not authenticated, an HTTP error `401` is returned
as described earlier.

Blacklist API
-------------

Blacklist a user already enrolled in the database. In case the user has
been identified to be a fraudster or a defaulter and needs to be
blacklisted, this API is called with the personId. After the
blacklisting operation, a search of the database with the user's face
should return the user's personId along with the flag indicating that
the user has been blacklisted.

If a user has been blacklisted already, then the user can be removed
from the blacklist, if required, by calling this same API call with the
optional parameter blacklist set to false.

> This API supports AppId Jouney. For details, please refer to the AppId
> Journey section.

### HTTP Request

`POST /person/blacklist`

> To make the API call

```js
    curl --location --request POST 'http://frsapi.fecredit.com.vn/v1.2/person/blacklist' \
    --header 'appkey: appKey' --header 'appsecret: appSecret' \
    --header "transactionId: transactionId" --header "referenceId: referenceId" \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "personId": "5e0b46d4-6dcc-4060-9340-01635d7bead5"
    }'
```

> The above command returns JSON structured like this:

```json
{
    "status" : "success",
    "statusCode" : "200"
}
```

### Query Parameters

|  Parameter  | Type |    Description |
|  ----------- | -------- | ----------------------------------------- |
|  personId   | STRING  | The personId whose blacklist needs to be changed. |
|  blacklist |  BOOL    | (Optional, Default: `true`) The blacklist flag. |

A successful API response will give the verification result as shown on
the right.

Below are the possible errors:

When a wrong request is made, an HTTP error `400` is returned. And it
could be because of below mentioned reasons, which is returned in the
status message.

-   Invalid personId: "E009 - Invalid personId"

When the request is not authenticated, an HTTP error `401` is returned
as described earlier.

NID Status Update API
---------------------

This API is used to update the correct / in-correct flag of an
application. This API would be helpful, if the application was created
by mistake and is a false application. Or the application was a fraud
application applied not by the customer but by someone on behalf of the
customer.

> This API supports AppId Jouney. For details, please refer to the AppId
> Journey section.

### HTTP Request

`POST /app/updateNID`

> To make the API call

```js
curl --location --request POST 'http://frsapi.fecredit.com.vn/v1.2/app/updateNID' \
    --header 'appkey: appKey' --header 'appsecret: appSecret' \
    --header "transactionId: transactionId" --header "referenceId: referenceId" \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "appId": "appId01",
        "state": false
    }'
```

> The above command returns JSON structured like this:

```json
{
    "status" : "success",
    "statusCode" : "200",
    "updateNID": {
        "appId": "correct_02",
        "newState": false,
        "oldState": true
    },
    "requestId": "ca7b82a0-fd2b-4d66-86a9-362ffb959c80"
}
```

### Query Parameters

|  Parameter |  Type |    Description |
|  ----------- | -------- | ------------------------------------------------ |
|  appId   |    STRING  | The appId whose state needs to be changed. |
|  state  |     BOOL    | The state as `true` or `false` which needs to be applied. |

A successful API response will give the verification result as shown on
the right.

Below are the possible errors:

When a wrong request is made, an HTTP error `400` is returned. And it
could be because of below mentioned reasons, which is returned in the
status message.

-   Invalid appId: "E006 - invalid application id"

When the request is not authenticated, an HTTP error `401` is returned
as described earlier.

PersonId - Search API
---------------------

This API is used to search through the database based on the `personId`
provided. It returns the list of application ids associated to this,
blacklist status of the person and optionally, the expanded details of
each applications.

### HTTP Request

`POST /search/person`

> To make the API call

```js
    curl --location --request POST 'http://frsapi.fecredit.com.vn/v1.2/search/person' \
    --header 'appkey: appKey' --header 'appsecret: appSecret' \
    --header "transactionId: transactionId" --header "referenceId: referenceId" \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "personId": "personId01",
        "expandAppIdDetails": "yes"
    }'
```

> The above command returns JSON structured like this:

``` json
{
    "status": "success",
    "statusCode": "200",
    "result": {
        "searchPerson": {
            "appIds": [ "99999902" ],
            "black": true,
            "csAppIds": [ "68218133" ],
            "appIdDetails": [
                {
                    "app_id": "99999902",
                    "correct_state": true,
                    "dob": "13-01-1998",
                    "gender": "M",
                    "name": "VAN HA",
                    "nid": "079999514004",
                    "person_id": "personId01"
                }
            ],
            "csAppIdDetails": [
                {
                    "app_id": "68218133",
                    "correct_state": true,
                    "nid": "",
                    "nid_id": "",
                    "person_id": "personId01"
                },
            ]
        }
    },
    "requestId": "291f6a16"
}
```

### Query Parameters

|  Parameter   |         Type  |   Description |
|  -------------------- | -------- | --------------------------------------------------------------- |
|  personId            | STRING  | The personId whose details are needed in the response. |
|  expandAppIdDetails  | STRING  | (Optional, Default: "no") If the result of each appIds are needed to be expanded, this paramter can be set to `yes`. |

A successful API response will give the verification result as shown on
the right.

Below are the possible errors:

When a wrong request is made, an HTTP error `400` is returned. And it
could be because of below mentioned reasons, which is returned in the
status message.

-   Invalid personId: "E009 - invalid personId"

When the request is not authenticated, an HTTP error `401` is returned
as described earlier.

AppId - Search API 
------------------

This API is used to search through the database on `appId`. It returns
the complete application details that were stored against the appId at
the time of its enrolment.

### HTTP Request

`POST /search/app`

> To make the API call

```js
    curl --location --request POST 'http://frsapi.fecredit.com.vn/v1.2/search/app' \
    --header 'appkey: appKey' --header 'appsecret: appSecret' \
    --header "transactionId: transactionId" --header "referenceId: referenceId" \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "appId": "appId01"
    }'
```

> The above command returns JSON structured like this:

```json
{
    "status": "success",
    "statusCode": "200",
    "result": {
        "searchApp": {
            "app_id": "appId01",
            "dob": "1984-04-26",
            "doi": "01/01/2008",
            "name": "Nguyen Hun",
            "person_id": "e6399a3f-d511-4985-92f7-747099ce023a"
        }
    },
    "requestId": "4437f4f3-5d0a-4176-a73f-8a9c1cd0dfe0"
}
```

### Query Parameters 

|   Parameter  |  Type    |  Description |
|   ----------- | -------- | -----------------------------------------------------|
|   appId     |   STRING  |  The appId whose details are needed in the response. |

A successful API response will give the verification result as shown on
the right.

Below are the possible errors:

When a wrong request is made, an HTTP error `400` is returned. And it
could be because of below mentioned reasons, which is returned in the
status message.

-   Invalid appId: "E009 - invalid appId"

When the request is not authenticated, an HTTP error `401` is returned
as described earlier.

Face - Search API
-----------------

This API is used to search through the face database using a selfie
image. This API is similar to the Dedupe API and returns a list of
possible matching faces. This API can also be used for enrolment purpose
for which separate parameter `enableEnrolment` is needed to be sent as
`yes`, which would return a face transaction Id in the response. This Id
can be used in the enrolment API along with the `profileData` data key
to send details about the customer.

### HTTP Request

`POST /search/face`

> To make the API call

```js
    curl --location --request POST 'http://frsapi.fecredit.com.vn/v1.2/search/face' \
    --header 'appkey: appKey' --header 'appsecret: appSecret' \
    --header "transactionId: transactionId" --header "referenceId: referenceId" \
    --form 'face=@im.png' \
    --form 'enableEnrolment=yes'
```

> The above command returns JSON structured like this:

``` json
{
    "status": "success",
    "statusCode": "200",
    "result": {
    "dedupe-result": [
            {
                "match-score" : <number>, // 0-100
                "selfie-image" : <base64 image of the selfie face crop>,
                "id-image" : <base64 image of the NID face crop>,
                "nationalId" : <string>,
                "name" : <string>,
                "details" : <otherDetailsJSON>
                "conf" : <number> //0-100,
                "blacklisted" : true/false,
                "personId" : <string>
            },
            ...
        ]
    },
    "requestId": "291f6a16"
}
```

### Query Parameters

|   Parameter       |   Type  |    Description |
|   ----------------- | -------- | --------------------------------------------------------- |
|   face          |     FILE   |   The face selfie image which would be used to search the database. |
|  enableEnrolment  | STRING  |  (Optional, Default: "no") To enable enrolment using face search API. |

A successful API response will give the verification result as shown on
the right.

Below are the possible errors:

When a wrong request is made, an HTTP error `400` is returned. And it
could be because of below mentioned reasons, which is returned in the
status message.

-   No Image uploaded - E002 - 1 image file input is required.
-   Larger than allowed image input: "E003 - image size cannot be
    greater than 6MB"
-   Incorrect formdata upload: "E011 - Upload Error : Unable to parse
    invalid formdata"

When the request is not authenticated, an HTTP error `401` is returned
as described earlier.

Merge API
---------

This API is used to merge different people as a single person. After
accepting a list of personIds, the API will merge those personIds into
single person and return the new personId allocated to them.

This API supports AppId Jouney. For details, please refer to the AppId
Journey section.

### HTTP Request

`POST /person/merge`

> To make the API call

```js
    curl --location --request POST 'http://frsapi.fecredit.com.vn/v1.2/person/merge' \
    --header 'appkey: appKey' --header 'appsecret: appSecret' \
    --header "transactionId: transactionId" --header "referenceId: referenceId" \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "people": [
            "23de8e2a-5842-4f89-a0dd-ad29d8798e0a", 
            "eaec0006-52b5-436f-a6b8-5090db218bd6"], 
        "caller": "CP",
        "meta": {
            "ch" : "PEGA",
            "ac" : "User Onboarding",
            "uId": "chandraUserId",
            "uCo": "chandra comments",
            "ma" : "yes"
        }
    }'
```

> The above command returns JSON structured like this:

```json
{
    "status": "success",
    "statusCode": "200",
    "result": {
        "personMerge": "e6399a3f-d511-4985-92f7-747099ce023a"
    },
    "requestId": "4437f4f3-5d0a-4176-a73f-8a9c1cd0dfe0"
}
```

### Query Parameters

|   Parameter  |  Type      |        Description |
|   ----------- -| --------------- | --------------------------------------------------------- |
|   people  |     LIST OF STRING  |  This is the list of people IDs which need to be merged. |

A successful API response will give the verification result as shown on
the right.

Below are the possible errors:

When a wrong request is made, an HTTP error `400` is returned. And it
could be because of below mentioned reasons, which is returned in the
status message.

-   Invalid personIds: "E009 - invalid people"

When the request is not authenticated, an HTTP error `401` is returned
as described earlier.

DeMerge API
-----------

This API is used to remove an appId from a personId, this API can be
used. It accepts an appId. The API will remove it from the person it
belonged to and return the new personId allocated to them. It returns
error, if the person to which belonged had only one appId which was
supplied.

This API supports AppId Jouney. For details, please refer to the AppId
Journey section.

### HTTP Request

`POST /person/demerge`

> To make the API call

```js
    curl --location --request POST 'http://frsapi.fecredit.com.vn/v1.2/person/demerge' \
    --header 'appkey: appKey' --header 'appsecret: appSecret' \
    --header "transactionId: transactionId" --header "referenceId: referenceId" \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "appId": "31"
    }'
```

> The above command returns JSON structured like this:

```json
{
    "status": "success",
    "statusCode": "200",
    "result": {
        "demergePeople": "e6399a3f-d511-4985-92f7-747099ce023a"     },
    "requestId": "4437f4f3-5d0a-4176-a73f-8a9c1cd0dfe0"
}
```

### Query Parameters

|   Parameter |   Type   |   Description |
|   ----------- | -------- | ---------------------------------------------------------- |
|   appId      |  STRING  |  The appId which has to be demerged from its main personId. |

A successful API response will give the verification result as shown on
the right.

Below are the possible errors:

When a wrong request is made, an HTTP error `400` is returned. And it
could be because of below mentioned reasons, which is returned in the
status message.

-   Invalid appId: "E009 - invalid appId"

When the request is not authenticated, an HTTP error `401` is returned
as described earlier.

Get AppID Journey
-----------------

This API is used to get the AppId Journey as described earlier.

### HTTP Request 

`POST /app/getJourney`

> To make the API call

```js
curl --location --request POST 'http://frsapi.fecredit.com.vn/v1.2/person/demerge' \
    --header 'appkey: appKey' --header 'appsecret: appSecret' \
    --header "transactionId: transactionId" --header "referenceId: referenceId" \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "appId": "31"
    }'
```

> The above command returns JSON structured like this:

```json| 
{
    "status": "success",
    "statusCode": "200",
    "result": {
        "journey": {
            "0": {
                "ac": "User Onboarding",
                "api": "enroll",
                "bl": false,
                "ch": "PEGA",
                "ma": "yes",
                "nidS": true,
                "oAIds": [
                    "111001"
                ],
                "pId": "f4035e0c-e7dc-4747-8946-173d094a0fdb",
                "ts": 1562042903000,
                "uCo": "chandra comments",
                "uId": "chandraUserId"
            }
        }
    },
    "requestId": "69fe382e-d9fc-46bd-836d-b0508f407698"
}
```

### Query Parameters

|   Parameter |   Type   |   Description |
|   ----------- | -------- | ------------------------------------------------------------ |
|   appId     |   STRING   The appId which has to be demerged from its main personId. |
| 
A successful API response will give the verification result as shown on
the right.

Below are the possible errors:

When a wrong request is made, an HTTP error `400` is returned. And it
could be because of below mentioned reasons, which is returned in the
status message.

-   Invalid appId: "E009 - invalid appId"

When the request is not authenticated, an HTTP error `401` is returned
as described earlier.

