---
title: V1
# hide:
#   - navigation # Hide navigation
 
---

# MasterDB and Face Recognition API (V1)

<!--
Notes : 
white board notes 
https://photos.app.goo.gl/lrPgPrjVymsCZuj83

Flow chart from lucid chart https://photos.app.goo.gl/zxzxnEN9ILbcR2d63

May 14th:
1. Remove pdf from supported types for image quality

May 6th comments from KV:

1. Process Flows
    1. Yellow Box - enroll API with link to existing user and blacklist set to false - done
    2. Would be good if we can mention the API call names on top of the box also - done
    3. Draw box around image quality check and mark image quality check API as well - done

2. Face Recognition API
    1. Change /person/dedupe to /person/verify - changed to face/recognize as per original
    2. How to use selfie, id AND dl in the request? Response accounts for multiple docs but request doesn’t specify how to send both docs - leaving it as it is, since we would mostly not support dl-selfie

May 4th TODO:

2. Change /face/recognize to /person/verify - changed to face/recognize
1. When to use what API from a business process point of view
	1. If possible add flow chart - done
2.  Change the face match to include multiple documents - done
2. Add OCR in different documentation - done
	1. List of documents 
	2. Fields that can be extracted 
		1. Add illustrations - is this needed ?
3. Answers to Puneet's questions in the documentation itself. - done

Future Todos : 
1. Document Database Schema - done
2. Update software architecture after final Schema and API design - TODO
3. Update hardware architecture - Add more specific details such as storage requirements based on clarity from 1. and 2. - TODO
-->

## Overview

This documentation describes the MasterDB and the Face Recognition API. It corresponds to **version 1.0** of the API.

1. Functionality
2. Process flows
2. API Schema
1. Root Endpoint
1. API Authentication
1. Media Types
3. API Endpoints
4. Error codes

## Functionality

High level functional specifications of the database and API are as follows: 

**MasterDB**

- Every existing customer file in the database has gone through the process of purification, de-duplication and facial biometric template creation
- The created facial templates are stored for real-time, accurate face recognition
- For all subsequent applicants, customer photos from any source such as Webcam, Mobile, etc can be used for recognition and enrollment

**Face Recognition API**

- The primary utility of the API is to identify if an applicant is an existing customer (either fradulent or good) or a new customer. This functionality is exposed through the Face Recognition API call
-  If it is a repeating customer, then the corresponding Customer Record in the Biometric Database will be updated with the latest Face biometric information. This is done through the Enrollment API call

<div style="page-break-after: always;"></div>

## Process Flows


#### Pega process flow

The new pega process proposed with the facial recognition system is as follows (v0.2). As can be seen in 3, the Facial checking is performed after the Image quality check and OCR.

<img src="../images/v1.0/pega process.png"
     alt="Markdown Monster icon"
     style="display:block;margin:auto;width:50%;" />

<div style="page-break-after: always;"></div>

<img src="../images/v1.0/pega-process.png"
     alt="Markdown Monster icon"
     style="display:block;margin:auto;" />

#### Detailed Facial Checking Flow
The output of the OCR run (in red) is provided as input to the Face recognition + dedupe system (in green). The Face recognition + dedupe system (in green) is performed by the Face recognition API, with the dedupe option enabled. 

The 'Generate new facial profile' feature (in blue) is performed by the enroll API. And the 'Generate new facial profile' + 'Black list' (in orange) is performed by the enroll API with the blacklist parameter set to true. To 'Black list exist customer' (in black), the blacklist API call is to be used.

![Facial checking flow](../images/v1.0/Facial Checking Risk flow from Request detail  v0.2.png)

#### Disbursement

For the disbursement flow that has been proposed. The Face Authentication API call can be used (Please refer documentation below).

![Disbursement flow](../images/v1.0/Disbursement.png)

## API Schema
The Face Recognition functionalities for the Master DB are exposed via REST APIs. All requests and responses are by default in JSON formats, and all image upload requests are to be performed as form-data (POST request).

## Root Endpoint
The root end point for **API version 1.0** of the production system is:

	https://frsapi.fecredit.com.vn/v1/

## API Authentication

All requests will be authenticated with an `apiKey` and `apiSecret`. The apiKey and apiSecret will need to provided in the request header. The requests would return a 401 error code incase the apiKey and apiSecret combination is invalid or not present. The apiKey can be used by different users to track their usage, and also be used for tracking overall usage of the client.

Please contact the Organization's Systems Administration team in order to obtain credentials for the on-premise production API.

## Media Types
The image formats supported by the APIs include PNG, JPEG, TIFF and PDF. The maximum file size accepted by the APIs is set to 6 MB. 

## API Endpoints

1. Face Recognition API
	2. Without dedupe
	3. With dedupe
2. Authentication
3. Image Quality
4. Enroll
5. Blacklist


### Face Recognition API

#### Without Dedupe (De-duplication)
Used to verify if the 2 face images belong to the same person. The 2 face images could be from different sources like national ID, selfie, web cam image, facebook etc. In case the verification performed is between an id and another image then file labels should be id/dl and selfie, else the labels need to be selfie and otherSelfie. 

If the face is detected in both the images, then the response would convey if the faces match along with the match-score, the confidence of the prediction and a to-be-reviewed flag that tells if the response requires a manual review. 

**Advanced :**
Depending on the FRR (false rejection rate) and FAR (false acceptance rate) required for automated review and willingness to perform manual review, the user can adopt an alternate review mechanism based on the confidence score. This can be arrived at by the user and HyperVerge working together. 

#### With Dedupe (De-duplication)
**In the "Detailed Facial Checking Flow" above, please refer to the steps 3, 4, 5, 6 in green. These steps are performed by the face recognition API when the `dedupe` option is set to true.** Combining these steps prevents repeated upload requests and also redundant conversions of PDF and face extraction from ID cards, hence, will greatly increase the capacity of the system.

If the optional parameter `dedupe` is set to true, then: 

A search is performed in the master face database for faces matching the footprint of the selfie and national id image in the request. The output would include the matches along with the details, this would be present as an array names dedupe-result. N would be identified based on the user's process at a later stage. The details will include the match-score and the confidence of the prediction. The other details along with the face image will also be retrieved for easy manual comparison.

In case dedupe is set to true, then additional parameters such as NID number, Name, DOB, Gender may be provided to perform detection of fraud that is not necessarily captured by face recognition based deduplication. **It is strictly advised to provide these to ensure better quality of output.** 

The **conf score** indicates the confidence of the prediction. If the conf is low, then it indicates that the Machine learning system is not confident of it's prediction. The **match score** indicates the degree to which the 2 faces are similar. If the score is 99 or 100 it means that the face image is exactly the same. Instead of using the match-score the user can rely on the "match" value which would be "yes" or "no", and instead of using the conf score the user can rely on the "to-be-reviewed" value which would be "yes" or "no".

* **URL** 
  - /face/recognize
  
<!--   - /person/dedupe -->
  
* **Method:**

    `POST`

* **Header**
	
	- content-type : 'formdata'
	- apiKey 
	- apiSecret
	
* **Request Body**
	
	- selfie : \<file\> or otherSelfie : \<file\>
	- selfie : \<file\> or id : \<file\>
	- selfie : \<file\> or dl : \<file\>
	
	optional:
	
	- dedupe : true / false \<default : false\>
		- if true (optional)
		 - nid
		 - name
		 - dob
		 - gender
		 - doi
		 - dob
		 - province 
		 - doe
		 - address
		 - retBase64Image (true / false)
	
* **Success Response:**

  * **Code:** 200 <br />
  * Incase of a properly made request, the response would follow schema.

		
		```
		{
			"status" : "success",
			"statusCode" : "200",
			"result" : {
				"verification-result" : [{
					"type" : "id-selfie/selfie-selfie/dl-selfie"
	                "match" : "yes/no",
	                "match-score" : <number>, // 0-100
	                "to-be-reviewed": "yes/no",
	                "conf" : <number>, //0-100 it is the confidence with which the prediction is correct
				}],
				"dedupe-result" : [
					{
		                "match-score" : <number>, // 0-100
		                "selfie-image" : <base64 image of the selfie face crop>,
		                "id-image" : <base64 image of the NID face crop>,
		                "nationalId" : <string>,
		                "name" : <string>,
		                "details" : <otherDetailsJSON>
		                "conf" : <number> //0-100,
		                "blacklisted" : true/false,
		                "personId" : <string>
					},
					...
				],
				"face-recognition-transaction-id" : <string>
				"is-dedupe-set" : true/false,
				"dedupe-to-be-reviewed": "yes/no"
			}
		}
		```

	* Incase of dedupe, there are 3 possible scenarios for each dedupe API output.
		* Confident positive match: This would return a single object in the dedupe result. This is the case where there is a person with a very similar face in the database. 
		* Confident negative match: In this case there would be 0 dedupe results. This is the case when there are no similar people at all.
		* Grey zone with less confidence: Top N dedupe-results would be returned for the purpose of manual review. In this case, there are multiple people in the database who are similar to the applicant and the system is not able to confidently identify if the applicant is present in the database and who it could be.
		* For each item in `dedupe-result`, the `selfie-image` and `id-image` is returned in the response only if `retBase64Image` is passed in the request payload and is set to `true`.

Face mismatch : Additionally, the dedupe API call will also report the applications that have the same National ID number if the selfies donot match.
		
  
```json
 **otherDetailsJSON Schema**
{
	"key" : {
		"fieldName" : <string>,
		"value" : <string>
	}
}
```


If the dedupe parameter is set to false in the request, then the "is-dedupe-set" is set to false and "dedupe-result" is null in the response.

* Incase of a properly made request, the response would follow the schema given below if the image does not contain a detectable face.

```json
{
	"status" : "success",
	"statusCode" : "200",
	"result" : {
		"error" : "E001 - Face not detected in one or more images",
	}
}
```
				
* **Error Response:**

* **Code:** 400 <br />

	`HTTP Status Code 400` is returned in case of request errors. The request errors would follow the Schema.

		{
		    "status": "failure",
		    "statusCode": "400",
		    "error": <errorMessage>		
		}

	1. No Image or 1 image input: "E002 - (selfie and id) or (selfie and otherSelfie) or (selfie and dl) are required"
	3. Larger than allowed image input: "E003 - image size cannot be greater than 6MB"
	4. Incorrect formdata upload: "E011 - Upload Error : Unable to parse invalid formdata"
			
	All error messages follow the same syntax with the status and statusCode also being a part of the response body, and a `string` error message with the description of the error.
	
* **Code:** 401 <br />
	
	Incase the API authentication fails. 
		
		
		{
			"status" : "failure",
			"statusCode" : "401"
		}

---
### Face Authentication API

Used to verify if the face in the query image matches with that in the image registered in the database for the given personId.

Difference between /person/verify without dedupe and Authenticate API

- /person/verify without dedupe : 2 input images are verified against each other.
- /face/authenticate : verifies a new input against the image used at the time of enrollment (refer enroll API). This would be used at a later stage of a customer life cycle when the customer returns to the mobile application or for any other purpose. **In the disbursement flow depicted above, the disbursement is performed when the user is authenticated against his appId with a photo, this functionality is what is implemented by /face/authenticate.**

* **URL**

  - /face/authenticate
  
* **Method:**

    `POST`

* **Header**
	
	- content-type : 'formdata'
	- apiKey 
	- apiSecret
	
* **Request Body**

	- personId : \<string\>
	- image : \<file\>
		
* **Success Response:**

  * **Code:** 200 <br />
  * Incase of a properly made request, the response would follow schema.

		
		```
		{
			"status" : "success",
			"statusCode" : "200",
			"result" : {
				"verification-result" : {
	                "match" : "yes/no",
	                "match-score" : <number>, // 0-100
	                "to-be-reviewed": "yes/no",
	                "conf" : <number>, //0-100 it is the confidence with which the prediction is correct
				}
			}
		}
		```
 	
 	* Incase of a properly made request, the response would follow schema if the image does not contain a detectable face.


		```
		{
			"status" : "success",
			"statusCode" : "200",
			"result" : {
				"error" : "E001 - Face not detected in input image",
			}
		}
		```
	
			
* **Error Response:**

* **Code:** 400 <br />

	`HTTP Status Code 400` is returned in case of request errors. The request errors would follow the Schema.
	
		{
		    "status": "failure",
		    "statusCode": "400",
		    "error": <errorMessage>		
		}
	
	1. No Image: "E002 - image is required"
	3. Larger than allowed image input: "E003 - image size cannot be greater than 6MB"
	4. Missing Param: "E004 - personId is required"
	5. Image format not supported: "E005 - invalid image format"
	6. User with the given personId not already enrolled: "E006 - invalid personId"
	7. Incorrect formdata upload: "E011 - Upload Error : Unable to parse invalid formdata"		
			
	All error messages follow the same syntax with the statusCode and status also being a part of the response body, and `string` error message with the description of the error.
	
* **Code:** 401 <br />
	
	Incase the API authentication fails. 
		
		
		{
			"status" : "failure",
			"statusCode" : "401"
		}

---
### Quality Check API
**Please refer to step 1 in the "Detailed Facial Checking Flow"**

Used to verify if the selfie image is of sufficient quality to perform face recognition. If the image is rejected, the user is required to capture another image. If the image is accepted by the quality check API, then the user can be assured that the face is detectable in the image and can proceed to verification or recognition steps. 

* **URL**

  - /photo/check 
  
* **Method:**

    `POST`

* **Header**
	
	- content-type : 'formdata'
	- apiKey 
	- apiSecret
	
* **Request Body**

	- image : \<file\>
	
* **Success Response:**

  * **Code:** 200 <br />

		
		```
		{
			"status" : "success",
			"statusCode" : "200",
			"result" : {
                "quality" : "good/bad"
			}
		}
		```

			
* **Error Response:**

	* **Code:** 400 <br />

		```
		{
			"status" : "failure",
			"statusCode" : "400",
			"error" : <errorMessage>
 		}
		```

	errorMessage could be one of the following strings:
		
	1. E002 - image is compulsory 
	2. E005 - supported image types include jpeg, tiff, png
	3. E003 - image size cannot be greater than 6MB 
	4. E011 - Upload Error : Unable to parse invalid formdata		
	
	* **Code:** 401 <br />
	
		Incase the API authentication fails. 
		
		```
		{
			"status" : "failure",
			"statusCode" : "401"
		}
		```
		
---
				
		
### Enrollment API

Enroll a new user or register a face to an existing user. After a new applicant is checked for repeat or blacklist user, he is required to be enrolled into the face database to ensure future search of the database returns the correct result for the applicant. 

In case of an existing user, registering their new image would help to keep the face database up to date. This is performed by providing the appId of the new applicant, the face-recognition-transaction-id and the personId from the face recognition (with dedupe) API response. By providing the personId, the new appId gets linked to the face in the personId. The user data and biometric facial footprint will be referenced from the face-recognition-transaction-id fed as input. **(Refer : Detailed Facial Checking Flow, the "Exist Customer" step highlighted in yellow for the corresponding case)**

If the user is being registered for the first time and is not to be blacklisted, the user needs to be enrolled but should not be linked with any existing user.**(Refer : Detailed Facial Checking Flow, the generate facial profile step highlighted in blue for the corresponding case)**

If the user who is being registered is to be blacklisted alongwith the registration, then the variable black needs to be set.**(Refer : Detailed Facial Checking Flow, the generate facial profile step + Blacklist step highlighted in orange for the corresponding step)**


<!-- The following table can be used as a reference to understand blacklisting and linking in different scenarios.

|New State|From DB|Enroll|Notes|
| ------ |--- |------ |---|
|Good|White|White, link|A good user who is also a repeat customer|
|Good|Black|White, No link|A user may be matched with a bad user from DB because of a fraudster in the past. In this case, where the current user is good, the new appId needs to be used to enroll the user without linking to existing user|
|Good|No Match|White, No link|A new user who is not to be blacklisted|
|Bad|White|Black, Link/No Link|In the case that the current appId has raised a suspicion/case of fraud, the user should be linked with the existing user based on whether the faces on the applications are the same|
|Bad|Black|Black, Link/No Link|In the case that the current appId has raised a suspicion/case of fraud, the user should be linked with the existing user based on whether the faces on the applications are the same|
|Bad|No Match|Black, No Link|A new user but has to be blacklisted|

In the above table there are references to **Bad/Good**. If you are going to blacklist a candidate or continue to keep the candidate as a blacklisted candidate, then he/she is a bad candidate. Else, the candidate would remain a good candidate -->

* **URL**

  - /person/enroll 
  
* **Method:**

    `POST`

* **Header**
	
	- content-type : 'application/json'
	- apiKey 
	- apiSecret
	
* **Request Body**

	- appId : \<string\>
	- black : true/false
	- face-recognition-transaction-id : \<string\>

	optional :
	 	
 	- personId : \<string\> 
		
* **Success Response:**

  * **Code:** 200 <br />

		
		```
		{
			"status" : "success",
			"statusCode" : "200",
			"result": {
				"personId" : "<string>"
			}
		}
		```
	 	
			
* **Error Response:**

* **Code:** 400 <br />

	`HTTP Status Code 400` is returned in case of request errors. The request errors would follow the Schema.
	
		{
		    "status": "failure",
		    "statusCode": "400",
		    "error": <errorMessage>		
		}
	
	1. Missing/invalid params: "E004 - missing required input params"
    2. Invalid transaction id:"E008 - Invalid face transaction id" 
    3. Invalid personId: "E009 - Invalid personId"
    4. E014 - "E014 - Invalid input data for blacklist"

	All error messages follow the same syntax with the statusCode and status also being a part of the response body, and `string` error message with the description of the error.
	
* **Code:** 401 <br />
	
	Incase the API authentication fails. 
		
		
		{
			"status" : "failure",
			"statusCode" : "401"
		}

---


### Blacklisting API

Blacklist a user already enrolled in the database. In case the user has been identified to be a fraudster or a defaulter and needs to be blacklisted, this API is called with the personId. After the blacklisting operation, a search of the database with the user's face should return the user's personId along with the flag indicating that the user has been blacklisted.

If a user has been blacklisted already, then the user can be removed from the blacklist, if required, by calling this same API call with the optional parameter `blacklist` set to false.

**(Refer : Detailed Facial Checking Flow, the "Black list exist customer" step highlighted in black for the corresponding case)**


* **URL**

  - /person/blacklist 
  
* **Method:**

    `POST`

* **Header**
	
	- content-type : 'application/json'
	- apiKey 
	- apiSecret
	
* **Request Body**

	- personId : string

	optional:
	
	- blacklist : true/false \<default : true\>
	
* **Success Response:**

  * **Code:** 200 <br />

		
		```
		{
			"status" : "success",
			"statusCode" : "200"
		}
		```

* **Error Response:**

* **Code:** 400 <br />

	User with the given personId not previously enrolled
			
		{
		    "status": "failure",
		    "statusCode": "400",
		    "error": "E009 - invalid personId"
		}		

	
* **Code:** 401 <br />
	
	Incase the API authentication fails. 
		
		
		{
			"status" : "failure",
			"statusCode" : "401"
		}
		
--- 

## Error Codes

1. E001 - face not detected in one or more images
2. E002 - input image is missing
3. E003 - input image is larger than limit
4. E004 - missing required input params
5. E005 - invalid image format
6. E006 - invalid appId	
7. E007 - face recognition call not executed for given appId
8. E008 - Invalid face transaction id
9. E009 - Invalid personId<!-- 10. EE010 - reserved for internal error in face-one-many--> 
10. E011 - Upload Error : Unable to parse invalid formdata <!-- E012 - reserved for internal error while inserting into DB in face-one-may E013 - reserved for internal REdis down-->
11. E014 - Invalid input data
<!-- E015 reserved for aerospike err, E016 GPU memory overflow-->
